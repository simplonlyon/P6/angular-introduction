## Mini TP : (./src/app/exo-template)
Faire un nouveau component avec un paragraphe dedans et 3 boutons, red/green/blue, quand on click sur le bouton, ça met le texte dans la couleur indiquée par le bouton
1. Créer un nouveau component ExoTemplateComponent
    * Créer un dossier exo-template
    * Dans ce dossier créer un fichier exo-template.component.ts et .html
    * Faire l'initialisation du component (déclarer dans le module et tout)
2. Faire appel à ce component dans le app.component.html
3. Dans la classe du component, créer une variable color initialisée à "black"
4. Créer une méthode changeColor(newColor) qui viendra changer la valeur de color par la nouvelle couleur
5. Dans le template, créer un paragraphe avec une directive [style.color] et lui assigner la variable color du component
6. Créer trois button html avec un event click qui déclenchera la méthode changeColor avec un argument différent selon le bouton
7. BONUS : Remplacer les trois boutons par un select avec trois options, une par couleur


## Exo Liste : (./src/app/exo-list)
Faire un component exo-list qui affichera une liste en ul/li dans le template, avec un input que quand on click sur un bouton, ça ajoute un nouvel élément à la liste

I- L'Affichage et l'ajout dans la liste
1. Générer un nouveau component ( ng g c mon-component) qu'on appelera exo-list
2. Aller l'afficher dans le app.component.html
3. Dans la classe du component, déclarer une variable list avec un ou deux élément textuel à l'intérieur
4. Dans le template, faire un petit for pour afficher la liste au format ul>li
5. Dans le component, rajouter une méthode addItem() qui viendra rajouter 'bloup' ou osef dans la liste
6. Dans le template, créer un bouton et lui rajouter un event au click qui déclenchera le addItem()
7. Dans le template rajouter un input text avec un ngModel lié à une variable dans la classe component
8. Modifier la méthode addItem() pour faire qu'elle ajoute dans la liste la valeur actuelle de la variable

II- Supprimer un élément de la liste
1. Dans le component, rajouter une méthode rmItem(index) qui supprime un élément de la liste par son index (avec .splice )
2. Dans le template, rajouter à chaque li un petit button qui, au click, déclenchera la méthode rm pour supprimer l'élément sur lequel on a cliqué

III- Réordonner la liste
1. Dans le component, rajouter une méthode changeOrder(index, direction) qui prendra en argument l'index de l'élément à déplacer et la direction dans lequel on veut le déplacer (up ou down par exemple) et qui viendra changer la position de l'élément en question pour le faire remonter d'un cran ou descendre d'un cran dans la liste (ça va demander un peu d'algo, vous pouvez chercher sur le net si jamais)
2. Dans le template, pour chaque élément de la liste, rajouter un bouton up et un bouton down qui feront respectivement monté ou descendre l'élément en question (en utilisant la méthode changeOrder() )
3. Faire que le bouton up ne s'affiche pas sur le premier élément et que le bouton down ne s'affiche pas sur le dernier élément

Mini Exo Router
Dans le projet angular-introduction, rajouter du routing pour naviguer entre les pages
1. Rajouter le module dans le AppModule
2. Créer la liste des routes, on en aura à priori 4 :
    * "first" pour le FirstComponent
    * "exo-li" pour le ExoLiComponent
    * "exo-template" pour le ExoTemplateComponent
    * "structure" pour le StructureComponent
3. Modifier le app.component.html pour y mettre le router outlet
4. Au dessus du router outlet, rajouter un ptit menu de navigation simpliste genre des a les uns à la suite des autres qui pointe sur chacune des pages