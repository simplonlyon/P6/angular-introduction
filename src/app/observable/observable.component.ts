import { Component, OnInit } from '@angular/core';
import { Observable,from } from 'rxjs';
import { AjaxService } from '../services/ajax.service';
import { Picture } from '../entity/picture';

@Component({
  selector: 'simplon-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {
  pictures:Picture[] = [];
  /**
   * Pour injecter un service dans un component, on fait comme
   * dans Symfony, on créer une propriété dans le constructeur et
   * on lui indique le type de donnée qu'on veut y injecter,
   * Angular s'occupe du reste.
   */
  constructor(private service:AjaxService) { }
  /**
   * La méthode ngOnInit sera déclenchée au chargement du component.
   * On y mettra toute l'algorithmie qu'on voudra exécutée lorsque
   * le component s'affichera.
   */
  ngOnInit() {
    //On peut ensuite utiliser le service et ses méthodes dans
    //le component
    this.service.findAllPicture()
    //On subscribe sur l'Observable renvoyé par la méthode et on
    //dit que lorsqu'il émet, on assigne la valeur émise (response)
    //à la propriété de notre component (this.pictures)
    .subscribe(response => this.pictures = response);

    //La méthode from de rxjs permet de créer un Observable à partir de
    //données ou de trucs (genre un tableau, un event etc.)
    let observableArray = from(['ga', 'zo', 'bu', 'meu']);

    observableArray.subscribe(
      //La première fonction sera exécutée à chaque émission de données par l'observable 
    function(data) {
      console.log(data);
    },
    //la deuxième fonction sera exécutée s'il y a une erreur dans l'observable
    function(error) {
      console.error(error);
    },
    //la troisième fonction sera exécutée quand l'observable aura terminé d'émettre
    function() {
      console.log('finished');
    });

    /**
     * On peut faire des Observable à partir de zéro (tout comme on
     * peut faire des Promise à partir de zéro [on le fait rarement])
     */
    let observableHomemade = Observable.create(function(observer) {
      
      for(let item of ['ga', 'zo', 'bu']) {
        //Le next indique une émission de donnée et la donnée à éméttre
        observer.next(item);
      }
      //le complete indique la fin de l'observable (il peut avoir un paramètre aussi)
      observer.complete();
    });

    observableHomemade.subscribe(
      data => console.log(data),
      error => console.error(error),
      () => console.log('finished')
      );


  }

}
