import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Picture } from '../entity/picture';
import { HttpClient } from '@angular/common/http';

/**
 * Le Injectable indique que cette classe sera gérée par Angular
 * et qu'on pourra donc y injecter des services Angular ou autre
 * Le providedIn indique à quel niveau le service sera disponible,
 * ici il le sera dès la racine, donc dans toute l'application.
 */
@Injectable({
  providedIn: 'root'
})
export class AjaxService {
  private url = 'http://localhost:8080/api/picture/';

  constructor(private http:HttpClient) { }

  findAllPicture(): Observable<Picture[]> {
    return this.http.get<Picture[]>(this.url);
  }
}
