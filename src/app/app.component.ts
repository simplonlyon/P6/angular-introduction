import { Component } from '@angular/core';
/**
 * Quand on génère une appli angular, le cli génère un
 * component AppComponent, qu'il prend comme component
 * racine par défaut. En général, on garde ce component
 * et on s'en sert pour y afficher nos components à nous
 * sans mettre trop de logique contrôleur dedans.
 * On va juste mettre nos components dans son template
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
}
